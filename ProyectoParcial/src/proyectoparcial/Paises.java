/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoparcial;

/**
 *
 * @author canss
 */
public class Paises {
    private String codigo, nombre;
    private int prefijo;

    public Paises(String codigo, String nombre, int prefijo) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.prefijo = prefijo;}

    public String getCodigo() {return codigo;}
    public void setCodigo(String codigo) {this.codigo = codigo;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public int getPrefijo() {return prefijo;}
    public void setPrefijo(int prefijo) {this.prefijo = prefijo;}}
