/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoparcial;

/**
 *
 * @author canss
 */
public class Ips {
    private String ip, tipo;
    private int codigo;

    public Ips(String ip, String tipo, int codigo) {
        this.ip = ip;
        this.tipo = tipo;
        this.codigo = codigo;}

    public String getIp() {return ip;}
    public void setIp(String ip) {this.ip = ip;}
    
    public String getTipo() {return tipo;}
    public void setTipo(String tipo) {this.tipo = tipo;}

    public int getCodigo() {return codigo;}
    public void setCodigo(int codigo) {this.codigo = codigo;}   
}
