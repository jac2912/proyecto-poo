/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoparcial;

/**
 *
 * @author canss
 */
public class Tarifas {
    private String codigo, codigoPais, nombreRegion;
    private int prefijoRegion;
    private double tarifa;
    private String codigoTarifa;

    public Tarifas(String codigo, String codigoPais, String nombreRegion, int prefijoRegion, double tarifa, String codigoTarifa) {
        this.codigo = codigo;
        this.codigoPais = codigoPais;
        this.nombreRegion = nombreRegion;
        this.prefijoRegion = prefijoRegion;
        this.tarifa = tarifa;
        this.codigoTarifa = codigoTarifa;
    }

    public String getCodigo() {return codigo;}
    public void setCodigo(String codigo) {this.codigo = codigo;}

    public String getCodigoPais() {return codigoPais;}
    public void setCodigoPais(String codigoPais) {this.codigoPais = codigoPais;}

    public String getNombreRegion() {return nombreRegion;}
    public void setNombreRegion(String nombreRegion) {this.nombreRegion = nombreRegion;}

    public int getPrefijoRegion() {return prefijoRegion;}
    public void setPrefijoRegion(int prefijoRegion) {this.prefijoRegion = prefijoRegion;}

    public double getTarifa() {return tarifa;}
    public void setTarifa(double tarifa) {this.tarifa = tarifa;}

    public String getCodigoTarifa() {return codigoTarifa;}
    public void setCodigoTarifa(String codigoTarifa) {this.codigoTarifa = codigoTarifa;}
}
