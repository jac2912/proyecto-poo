/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoparcial;

/**
 *
 * @author jac2912
 */
public class Clientes {
    private String codigo, nombre, direccion;
    private long telefono;
    private String tipoCliente;

    public Clientes(String codigo, String nombre, String direccion, long telefono, String tipoCliente) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.tipoCliente = tipoCliente;
    }

    public String getCodigo() {return codigo;}
    public void setCodigo(String codigo) {this.codigo = codigo;}

    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}

    public String getDireccion() {return direccion;}
    public void setDireccion(String direccion) {this.direccion = direccion;}

    public long getTelefono() {return telefono;}
    public void setTelefono(long telefono) {this.telefono = telefono;}

    public String getTipoCliente() {return tipoCliente;}
    public void setTipoCliente(String tipoCliente) {this.tipoCliente = tipoCliente;}    
}
