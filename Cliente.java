/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.pkg1;

/**
 *
 * @author User1
 */
public class Cliente {
    private String nombre;
    private String codigo;
    private String direccion;
    private int telefono;
    private String tipo_cliente;

    public Cliente(String nombre, String codigo, String direccion, 
                 double telefono, String tipo_cliente)
    {
      this.nombre=nombre;
      this.codigo=codigo;  
      this.direccion=direccion;
      this.telefono=(int) telefono;
      this.tipo_cliente=tipo_cliente;
    }
   
    public String getnombre() 
    {
        return nombre;
    }
    public void setnombre(String nombre)
    {
        this.codigo=nombre;
    }
    public String getcodigo() 
    {
        return codigo;
    }
    public void setcodigo(String codigo)
    {
        this.codigo=codigo;
    }
    public String getdireccion() 
    {
        return direccion;
    }
    public void setdireccion(String direccion)
    {
        this.codigo=direccion;
    }
    public int gettelefono() 
    {
        return telefono;
    }
    public void settelefono(int telefono)
    {
        this.telefono=telefono;
    }
    public String gettipo_cliente() 
    {
        return tipo_cliente;
    }
    public void settipo_cliente(String tipo_cliente)
    {
        this.tipo_cliente=tipo_cliente;
    }
    
}
    
